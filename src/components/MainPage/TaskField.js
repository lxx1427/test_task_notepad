import React from 'react';

const TaskField = ({value, name, format}) => 
    value ? <p>{name}: <span>{format ? format(value): value}</span></p> : '';

export default TaskField;