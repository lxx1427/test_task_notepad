
import { changeNameActionType, CHANGE_NAME } from '../actions';

test('should return action creater CHANGE_NAME', () => {
    const data = { id: 1, name: 'task name' }
    const expectedAction = { type: CHANGE_NAME, data }

    expect(changeNameActionType(data)).toEqual(expectedAction);
})