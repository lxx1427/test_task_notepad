export const changeApiName = (data) => { 
        return fetch('https://photos.google.com/', {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },  
        body: JSON.stringify({data})
    }).then((response) => {
        return response.json
    })
}