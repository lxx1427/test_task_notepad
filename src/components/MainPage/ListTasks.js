import React from 'react';
import TaskPreview from './TaskPreview';

const ListTasks = ({tasks, onPreview}) => 
    tasks.map( 
        item => (
            item.obj_status ==='active' && <TaskPreview 
                    key = {item.id}
                    item = {item}
                    onPreview = {onPreview}
                />
        )
    )


export default ListTasks;