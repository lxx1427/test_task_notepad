import data from '../../system/data.json'
import { CHANGE_NAME } from './actions';

export const initialState = {
    tasks: data,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case CHANGE_NAME:        
            const { id, name } = action.data;
            return {
                ...state,
                tasks: state.tasks.map(task =>
                    task.id === id
                        ? { ...task, name }
                        : task
                )
            }
        default: return state

    }
}