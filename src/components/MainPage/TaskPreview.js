import React from 'react';
import { formatDate, formatText } from '../../system/utilites';
import { Link } from 'react-router-dom';
import TaskField from './TaskField';

const TaskPreview = (props) => {
    const {
        id,
        is_high_priority,
        tags, actual_effort,
        estimated_effort,
        due_date,
        name,
    } = props.item;
    const currentClass = is_high_priority ? 'task-name high-priority-task' : 'task-name';
    return (
        <div className='task-container'>
            <h3 className={currentClass}><Link to={`/task/${id}`} >{name}</Link></h3>
            <TaskField name={'Tags'} value={tags} format={formatText}/>
            <TaskField name={'Actual effort'} value={actual_effort}/>
            <TaskField name={'Estimated effort'} value={estimated_effort}/>
            <TaskField name={'Due date'} value={due_date} format={formatDate}/>
        </div>
    )
}

export default TaskPreview;