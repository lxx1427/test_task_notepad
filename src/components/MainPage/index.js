import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListTasks from './ListTasks';
import './MainPage.scss';


class MainPage extends Component {

    render() {
        return(
            <div className = "main-page">
                <ListTasks tasks={this.props.tasks}/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.mainPage.tasks
})

export default connect(mapStateToProps)(MainPage);
