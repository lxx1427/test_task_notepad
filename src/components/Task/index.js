import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getTask } from './selectors';
import { changeNameApiAction } from '../MainPage/actions';
import { Link } from 'react-router-dom';
import './Task.scss';

class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.task.name
        }
    }
    componentDidMount() {
        this._inputName.onblur=this.onBlurChange;
    }

    handlerChange = (e) => {
        const value = e.target.value;
        this.setState({ value });
    }

    onBlurChange = () => {
        const { value } = this.state;
        const { id } = this.props.task;
        this.props.onChangeName({id, name: value})
    }

    render() {
        const { description } = this.props.task;
        return (
            <div className='single-task'>
                <input
                    ref ={node => {this._inputName = node}}
                    className="task-rename-field"
                    type="text"
                    onChange={this.handlerChange}
                    value={this.state.value} />
                    <Link to='/'><i className="fas fa-arrow-alt-circle-left"></i></Link>
                <p>Description: <span>{description}</span></p>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {

    const taskId = ownProps.match.params.id;

    return {
        task: getTask(state, taskId)
    }
}
const mapDispatchToProps = (dicpatch) => (
    {
        onChangeName: data => changeNameApiAction(dicpatch, data)
    }
)

export default connect(mapStateToProps, mapDispatchToProps)(Task);