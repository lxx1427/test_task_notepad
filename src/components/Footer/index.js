import React from 'react';
import './Footer.scss';

const Footer = () => {
    return(
      <div className = "footer-container">
        <h4>This App was created by Alexey</h4>
        <i className="fas fa-atom"></i>
      </div>
    )
  }
  
export default Footer;