import React from 'react';
import './Header.scss';


const Header = () => {
  return(
    <div className = "header-container">
      <i className="fas fa-pencil-alt"></i>
      <h2>My NoteBook</h2>
    </div>
  )
}

export default Header;