import React from 'react';
import { Switch, Route } from 'react-router-dom';
import MainPage from '../MainPage';
import Task from '../Task';

export default function () {
    return (
        <Switch>
            <Route exact path='/' component={MainPage} />
            <Route path='/task/:id' component={Task} />
        </Switch>
    )
}