import { combineReducers } from 'redux';

import mainPage from '../components/MainPage/reducer';

export default combineReducers({
    mainPage
})