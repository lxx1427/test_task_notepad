export const formatDate = value => new Date(value).toLocaleDateString();

export const formatText = value => value.join(', ');
