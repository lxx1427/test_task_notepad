import React, { Component } from 'react';
import Header from './components/Header';
import Footer from './components/Footer';
import Router from './components/Router';
import './App.scss';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Header />
          <Router/>
        <Footer />
      </div>
    );
  }
}

export default App;
